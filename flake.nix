{
  description = "My network as code using Terraform and Nix";

  # In principle, if we let the transitive nixpkgs inputs follow the main nixpkgs inputs, we can
  # reduce build times and sizes as well as receive security updates. However, this comes at a
  # potential cost of breakage if the versions do not work together, so we have to be careful.
  inputs = {
    devenv.url = "github:cachix/devenv";
    flake-utils.url = "github:numtide/flake-utils";
    hardware.url = "github:nixos/nixos-hardware";
    home-manager = {
      url = "github:nix-community/home-manager/release-23.05";
      inputs.nixpkgs.follows = "nixpkgs-23_05";
    };
    hyprland.url = "github:hyprwm/hyprland";
    lollypops = {
      url = "github:troyfigiel/lollypops";
      inputs.nixpkgs.follows = "nixpkgs-23_05";
    };
    nixpkgs-23_05.url = "github:nixos/nixpkgs/nixos-23.05";
    emacs-config.url = "gitlab:troyfigiel/emacs-config";
    website.url = "gitlab:troyfigiel/website";
  };

  outputs = { flake-utils, lollypops, nixpkgs-23_05, self, ... }@inputs:
    {
      nixosModules = { imports = import ./configuration/roles/list.nix; };
      nixosConfigurations = import ./configuration { inherit inputs; };
    }

    //

    (flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs-23_05.legacyPackages.${system};
      in {
        apps.default = lollypops.apps.${system}.default { configFlake = self; };

        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [ black nil nixfmt python3 terraform ];
        };

        checks = {
          deadnix = pkgs.runCommand "checks-deadnix" {
            buildInputs = [ pkgs.deadnix ];
          } ''
            deadnix --fail ${./.}
            touch $out
          '';

          statix = pkgs.runCommand "checks-statix" {
            buildInputs = [ pkgs.statix ];
          } ''
            statix check ${./.} 2>/dev/null
            touch $out
          '';

          nixfmt = pkgs.runCommand "checks-nixfmt" {
            buildInputs = [ pkgs.findutils pkgs.nixfmt ];
          } ''
            find ${./.} -type f -name '*.nix' -print0 | xargs -0 nixfmt -c
            touch $out
          '';
        };

        formatter = pkgs.writeShellApplication {
          name = "formatter-nixfmt";
          runtimeInputs = [ pkgs.findutils pkgs.nixfmt ];
          text = ''
            find "$@" -type f -name '*.nix' \
                 -not -path '**/.git/*' \
                 -print0 \
                 | xargs -0 nixfmt
          '';
        };
      }));
}
