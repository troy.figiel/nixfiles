{ lib, machines, pkgs, thisMachine, ... }:

let
  inherit (lib) mkIf mkMerge;
  mountDir = "/storage";
in mkMerge [
  {
    environment.systemPackages = with pkgs; [ btrfs-progs compsize ];

    # TODO: I should extract out this path, because it acts as the interface to the raid1 btrfs file
    # system.
    fileSystems.${mountDir} = {
      device = "/dev/disk/by-label/storage";
      fsType = "btrfs";
      options = [ "noatime" "nofail" "autodefrag" "compress=zstd" ];
    };

    services.btrfs.autoScrub = {
      enable = true;
      fileSystems = [ mountDir ];
      interval = "Mon, 1:00";
    };
  }

  (mkIf (thisMachine == machines.nas) {
    systemd.services."deduplicate" = {
      # We only run duperemove on the export volume, because potential performance issues do not
      # play a big role here if we pick our block size large enough.
      script = ''
        ${pkgs.fdupes}/bin/fdupes -qr /storage/export | ${pkgs.duperemove}/bin/duperemove --fdupes
      '';
      # We need the lscpu binary from the util-linux package to be available, so duperemove can
      # check how many cores it should use.
      path = [ pkgs.util-linux ];
      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
    };

    systemd.timers."deduplicate" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "monthly";
        Unit = "deduplicate.service";
      };
    };
  })
]
