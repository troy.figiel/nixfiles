{ inputs, ... }:

{
  imports = [ ./btrfs inputs.hardware.nixosModules.raspberry-pi-4 ];

  powerManagement.cpuFreqGovernor = "ondemand";

  # When using U-Boot, the boot files are stored on the main partition instead of the firmware
  # partition.
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };

  nixpkgs.hostPlatform = "aarch64-linux";
}
