{ inputs, lib, pkgs, ... }:

{
  nix = {
    package = pkgs.nixVersions.stable;
    settings.auto-optimise-store = true;

    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };

    extraOptions = "experimental-features = nix-command flakes";
  };

  nixpkgs = {
    config = {
      allowUnfreePredicate = pkg:
        builtins.elem (lib.getName pkg) [
          "minecraft-server"
          "skypeforlinux"
          "steam"
          "steam-original"
          "steam-run"
        ];
      allowUnsupportedSystem = true;
    };
    overlays = [
      (_final: _prev: {
        tf-emacs = inputs.emacs-config.packages.${pkgs.system}.default;
        website = inputs.website.packages.${pkgs.system}.default;
        devenv = inputs.devenv.packages.${pkgs.system}.default;
      })
    ];
  };
}
