{ lib, machines, pkgs, thisMachine, ... }:

let inherit (lib) elem optionals;
in {
  environment.systemPackages = with pkgs;
    [ btop dtach git tldr vim wget ]
    ++ optionals (elem thisMachine [ machines.nas machines.raspberry-pi ]) [
      e2fsprogs
      iotop
      parted
      smartmontools
    ];

  system.stateVersion = "23.05";
}
