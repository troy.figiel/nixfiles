{ lib, thisMachine, ... }:

let inherit (lib) mkDefault;
in {
  networking = {
    inherit (thisMachine) hostName;
    firewall.enable = true;

    # TODO: I need to set this to true, otherwise I will lock myself out of my raspberry pi. At
    # some point I changed this value to false and got locked out the moment my router refreshed
    # the IP addresses. Nonetheless, it seems the more "modern" option is to set DHCP per
    # interface. I should have a look into this.
    useDHCP = mkDefault true;
  };
}
