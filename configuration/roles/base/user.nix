{ admin, inputs, lib, machines, pkgs, thisMachine, ... }:

let
  inherit (lib) elem mkIf mkMerge;
  shouldHaveHome = elem thisMachine [ machines.laptop machines.nas ];
in {
  imports = [ inputs.home-manager.nixosModules.home-manager ];

  config = mkMerge [
    {
      users = {
        defaultUserShell = pkgs.bash;
        groups.users.gid = 100;
        users.${admin.user} = {
          group = "users";
          uid = 1000;
          isNormalUser = true;
          createHome = shouldHaveHome;
          extraGroups = [ "wheel" ];
        };
      };
    }

    (mkIf shouldHaveHome {
      home-manager = {
        useGlobalPkgs = true;
        useUserPackages = true;
        users.${admin.user}.home = {
          username = admin.user;
          homeDirectory = "/home/${admin.user}";
          stateVersion = "23.05";
        };
      };
    })
  ];
}
