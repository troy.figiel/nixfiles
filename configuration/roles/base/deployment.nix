{ admin, inputs, pkgs, thisMachine, ... }:

{
  imports = [ inputs.lollypops.nixosModules.lollypops ];

  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = false;
    hostKeys = [{
      type = "rsa";
      bits = 4096;
      path = "/etc/ssh/ssh_host_rsa_key";
    }];
  };
  users.users.root.openssh.authorizedKeys.keys = [ admin.keys.ssh ];

  # Git and rsync are required for lollypops to work.
  environment.systemPackages = with pkgs; [ git rsync ];
  lollypops.deployment.ssh.host = thisMachine.ipAddress;
}
