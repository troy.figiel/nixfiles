{ config, lib, machines, pkgs, terraformOutputs, thisMachine, ... }:

let
  inherit (lib) elem mkIf mkMerge;
  port = 8000;
in mkMerge [
  (mkIf (thisMachine == machines.raspberry-pi) {
    lollypops.secrets.files."restic" = { cmd = "pass restic"; };

    services.restic.server = {
      enable = true;
      appendOnly = true;
      dataDir = "/storage/restic/";
      listenAddress = ":${toString port}";
      extraFlags = [ "--no-auth" ];
    };

    networking.firewall.allowedTCPPorts = [ port ];

    systemd.services."restic-full-check" = {
      script = ''
        ${pkgs.restic}/bin/restic check -r /storage/restic/ -p ${
          config.lollypops.secrets.files."restic".path
        } --read-data
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
    };

    systemd.timers."restic-full-check" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "Sat, 4:00";
        Unit = "restic-full-check.service";
      };
    };

    systemd.services."restic-short-check" = {
      script = ''
        ${pkgs.restic}/bin/restic prune -r /storage/restic/ -p ${
          config.lollypops.secrets.files."restic".path
        }
        ${pkgs.restic}/bin/restic check -r /storage/restic/ -p ${
          config.lollypops.secrets.files."restic".path
        }
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
    };

    systemd.timers."restic-short-check" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "3:00";
        Unit = "restic-short-check.service";
      };
    };

    lollypops.secrets.files."b2_application_key" = {
      cmd = "pass b2_application_key";
    };
    lollypops.secrets.files."b2_application_key_id" = {
      cmd = "pass b2_application_key_id";
    };

    # TODO: Use a different API key for rclone than the default one.
    systemd.services."rclone-storage" = {
      # TODO: Can I rewrite this using `systemd.services.<name>.environment`?
      script = ''
        export RCLONE_CONFIG_B2_TYPE="b2"
        export RCLONE_CONFIG_B2_ACCOUNT="$(cat ${
          config.lollypops.secrets.files."b2_application_key_id".path
        })"
        export RCLONE_CONFIG_B2_KEY="$(cat ${
          config.lollypops.secrets.files."b2_application_key".path
        })"
        ${pkgs.rclone}/bin/rclone sync -v "/storage/snapshots/restic.$(date '+%Y%m%d')/" b2:${terraformOutputs.restic-bucket}
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
    };

    # TODO: This should only trigger if restic backup ran successfully. I should be able to
    # implement this type of dependency in the timers.
    systemd.timers."rclone-storage" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "5:00";
        Unit = "rclone-storage.service";
      };
    };

    environment.systemPackages = [ pkgs.rclone ];
  })

  (mkIf (thisMachine == machines.nas) {
    lollypops.secrets.files."restic" = { };

    services.restic.backups.storage = let snapshotDir = "/storage/backup";
    in {
      paths = [ snapshotDir ];
      initialize = true;
      repository =
        "rest:http://${machines.raspberry-pi.hostName}:${toString port}/";
      passwordFile = config.lollypops.secrets.files."restic".path;
      backupPrepareCommand = let
        subvolumeSnapshot = "${pkgs.btrfs-progs}/bin/btrfs subvolume snapshot";
      in ''
        ${pkgs.coreutils}/bin/mkdir -vp ${snapshotDir}/{export,services}
        ${subvolumeSnapshot} -r /storage/export/nfs ${snapshotDir}/export/nfs
        ${subvolumeSnapshot} -r /storage/export/smb ${snapshotDir}/export/smb
        ${subvolumeSnapshot} -r /storage/services/gitea ${snapshotDir}/services/gitea
        ${subvolumeSnapshot} -r /storage/services/minecraft ${snapshotDir}/services/minecraft
        ${subvolumeSnapshot} -r /storage/services/minio ${snapshotDir}/services/minio
        ${subvolumeSnapshot} -r /storage/services/minio ${snapshotDir}/services/paperless
        ${subvolumeSnapshot} -r /storage/services/traefik ${snapshotDir}/services/traefik
      '';
      backupCleanupCommand =
        let subvolumeDelete = "${pkgs.btrfs-progs}/bin/btrfs subvolume delete";
        in ''
          ${subvolumeDelete} ${snapshotDir}/export/nfs
          ${subvolumeDelete} ${snapshotDir}/export/smb
          ${subvolumeDelete} ${snapshotDir}/services/gitea
          ${subvolumeDelete} ${snapshotDir}/services/minecraft
          ${subvolumeDelete} ${snapshotDir}/services/minio
          ${subvolumeDelete} ${snapshotDir}/services/paperless
          ${subvolumeDelete} ${snapshotDir}/services/traefik
          ${pkgs.coreutils}/bin/rmdir -v ${snapshotDir}/{export,services} ${snapshotDir}
        '';
      timerConfig.OnCalendar = "daily";
    };
  })

  (mkIf (elem thisMachine (with machines; [ laptop nas raspberry-pi ])) {
    environment.systemPackages = [ pkgs.restic ];
  })
]
