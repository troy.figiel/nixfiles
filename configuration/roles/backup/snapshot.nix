{ lib, machines, thisMachine, ... }:

let inherit (lib) mkIf mkMerge;
in mkMerge [
  (mkIf (thisMachine == machines.raspberry-pi) {
    # We use btrbk to automatically store snapshots of the Restic repository. This ensures a
    # rollback is possible if the repository ever breaks.
    services.btrbk = {
      instances.btrbk = {
        # TODO: This needs to be scheduled after the backup has been received. Is there a more
        # obvious way to indicate this except by scheduling onCalendar after the restic backup?
        onCalendar = "2:00";
        settings = {
          snapshot_preserve_min = "latest";
          snapshot_preserve = "14d 12w 12m *y";
          timestamp_format = "short";
          volume."/storage" = {
            snapshot_dir = "snapshots";
            subvolume."restic" = { };
          };
        };
      };
    };
  })

  (mkIf (thisMachine == machines.nas) {
    services.btrbk = {
      instances.btrbk = {
        onCalendar = "hourly";
        settings = {
          # These settings ensure I will never have more than ~30 snapshots per subvolume. This
          # should be more than enough for any kind of recovery purposes where either my Restic
          # backup has not run yet or for some reason I cannot recover any data from any of my
          # repositories anymore.
          snapshot_preserve_min = "latest";
          snapshot_preserve = "12h 7d 4w 6m";
          volume."/storage" = {
            snapshot_dir = "snapshots";
            subvolume = {
              "export/nfs" = { };
              "export/smb" = { };
              "services/gitea" = { };
              "services/minecraft" = { };
              "services/minio" = { };
              "services/paperless" = { };
              "services/traefik" = { };
            };
          };
        };
      };
    };
  })
]
