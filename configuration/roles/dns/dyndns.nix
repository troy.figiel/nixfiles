{ config, lib, machines, thisMachine, ... }:

let inherit (lib) mkIf;
in mkIf (thisMachine == machines.nas) {
  lollypops.secrets.files."cloudflare/api-token-file" = { };

  services.cloudflare-dyndns = {
    enable = true;
    apiTokenFile =
      config.lollypops.secrets.files."cloudflare/api-token-file".path;
    domains = [ "home.troyfigiel.com" ];
  };
}
