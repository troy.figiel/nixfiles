{ lib, ... }:

let inherit (lib) mkIf;
in mkIf false { services.dnsmasq = { enable = true; }; }
