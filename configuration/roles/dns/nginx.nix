{ lib, machines, thisMachine, ... }:

let inherit (lib) mkIf;
in mkIf (thisMachine == machines.nas) {
  services.nginx = {
    enable = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;

    # This sets the default response if there are no exact matches. If this is not set, nginx might
    # forward the request to a random application.
    virtualHosts."_".locations."/".return = "444";
  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];
}
