{ admin, config, lib, machines, pkgs, thisMachine, ... }:

let inherit (lib) elem mkIf mkMerge optional;
in mkIf (elem thisMachine (with machines; [ laptop nas ])) {
  home-manager.users.${admin.user} = {
    home.packages = optional (thisMachine == machines.laptop) pkgs.devenv;

    # TODO: I am not sure what this is doing anymore. Why do we have
    # all the userDirs in sessionVariables?
    home.sessionVariables = let
      inherit (config.home-manager.users.${admin.user}.xdg) userDirs;
      inherit (userDirs) documents;
      projects =
        "${userDirs.extraConfig.XDG_PROJECTS_DIR}/gitea.local.troyfigiel.com/troyfigiel";
    in mkMerge [
      {
        # TODO: Remove this when I set up better integration between
        # vterm and Emacs
        EDITOR = "vim";
      }

      (mkIf (thisMachine == machines.laptop) {
        # TODO: This should only be set on my laptop, not on my NAS.
        # This is a bit of a hack.
        NOTES_DIR = "${projects}/notes";
        LIBRARY_DIR = "${documents}/library";
        ORG_DIR = "${projects}/org";
        EMACS_DATA_DIR = "/home/${admin.user}/nfs/applications/emacs";
      })
    ];
    programs = {
      # I need to explicity enable bash to have a working direnv
      # integration outside of Emacs.
      bash.enable = true;
      dircolors.enable = true;
      direnv = {
        enable = true;
        nix-direnv.enable = true;
      };
    };
  };
}
