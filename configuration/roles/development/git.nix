{ admin, config, lib, machines, pkgs, thisMachine, ... }:

let inherit (lib) mkIf;
in mkIf (thisMachine == machines.laptop) {
  home-manager.users.${admin.user} = {
    home.packages = [ pkgs.ghq ];

    programs.git = {
      enable = true;
      userName = "Troy Figiel";
      userEmail = "troy.figiel@gmail.com";
      signing = { key = "E47C0DCD2768DFA138FCDCD6C67C9181B3893FB0"; };
      extraConfig = {
        init.defaultBranch = "main";
        rerere.enabled = true;
        commit.verbose = true;
        pull.rebase = true;
        push.autoSetupRemote = true;
        ghq.root =
          config.home-manager.users.${admin.user}.xdg.userDirs.extraConfig.XDG_PROJECTS_DIR;
      };
    };
  };
}
