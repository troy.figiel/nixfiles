{ admin, lib, machines, thisMachine, ... }:

let inherit (lib) mkIf mkMerge;
in mkMerge [
  (mkIf (thisMachine == machines.nas) {
    users.users.${admin.user}.openssh.authorizedKeys.keys = [ admin.keys.ssh ];
  })

  (mkIf (thisMachine == machines.laptop) {
    home-manager.users.${admin.user}.programs.ssh = {
      enable = true;
      matchBlocks.nas = {
        localForwards = [{
          bind.port = 8888;
          host.address = "localhost";
          host.port = 8888;
        }];
      };
    };
  })
]
