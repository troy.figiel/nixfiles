{ admin, lib, machines, thisMachine, ... }:

let
  inherit (lib) elem optional;
  isLaptop = (thisMachine == machines.laptop);
  isLaptopOrNas = (elem thisMachine (with machines; [ laptop nas ]));
in {
  virtualisation = {
    virtualbox.host.enable = isLaptop;
    multipass.enable = isLaptop;
    docker.enable = isLaptopOrNas;
  };
  users.users.${admin.user}.extraGroups = (optional isLaptop "vboxusers")
    ++ (optional isLaptopOrNas "docker");
}
