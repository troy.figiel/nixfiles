{ lib, machines, thisMachine, ... }:

let inherit (lib) mkIf;
in mkIf (thisMachine == machines.cloud-server) {
  services.fail2ban = {
    enable = true;
    maxretry = 5;
    bantime-increment = {
      enable = true;
      maxtime = "48h";
    };
  };
}
