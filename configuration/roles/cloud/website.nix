{ admin, lib, machines, pkgs, terraformOutputs, thisMachine, ... }:

let
  inherit (lib) mkIf;
  domain = terraformOutputs.troyfigiel-domain;
in mkIf (thisMachine == machines.cloud-server) {
  services.nginx = {
    enable = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
    virtualHosts.${domain} = {
      forceSSL = true;
      enableACME = true;
      serverAliases = [ "www.${domain}" ];
      locations."/" = { root = pkgs.website; };
    };
  };

  security.acme = {
    acceptTerms = true;
    defaults.email = admin.email;
  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];
}
