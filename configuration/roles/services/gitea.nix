{ config, lib, machines, pkgs, terraformOutputs, thisMachine, ... }:

let
  inherit (lib) mkIf;
  port = 3000;
  user = "git";
  domain = "gitea.local.${terraformOutputs.troyfigiel-domain}";
in mkIf (thisMachine == machines.nas) {
  lollypops.secrets.files."gitea.troyfigiel.com/tokenfile" = { };

  # To use ghq, I need to be able to clone with ssh://git@nas. The
  # default user for gitea should therefore be renamed to git.
  users.users.${user} = {
    description = "Gitea Service";
    home = config.services.gitea.stateDir;
    useDefaultShell = true;
    group = user;
    isSystemUser = true;
  };

  users.groups.${user} = { };

  services.gitea = {
    inherit user;
    group = user;
    enable = true;
    settings = {
      server = {
        DOMAIN = thisMachine.hostName;
        ROOT_URL = "http://${domain}";
      };

      actions.ENABLED = true;
    };
    database = {
      inherit user;
      name = user;
    };
    appName = "Troy's Gitea server";
    stateDir = "/storage/services/gitea";
  };

  services.gitea-actions-runner.instances.${thisMachine.hostName} = {
    enable = true;
    name = thisMachine.hostName;
    url = config.services.gitea.settings.server.ROOT_URL;
    labels = [
      # provide a debian base with nodejs for actions
      "debian-latest:docker://node:18-bullseye"

      # fake the ubuntu name, because node provides no ubuntu builds
      "ubuntu-latest:docker://node:18-bullseye"

      # provide native execution on the host
      "native:host"
    ];
    hostPackages = with pkgs; [
      bash
      coreutils
      curl
      gawk
      gitMinimal
      gnused
      nodejs
      wget
    ];
    tokenFile =
      config.lollypops.secrets.files."gitea.troyfigiel.com/tokenfile".path;
  };

  services.nginx.virtualHosts.${domain}.locations."/" = {
    proxyPass = "http://${thisMachine.ipAddress}:${toString port}";
    proxyWebsockets = true;
  };

  # TODO: I do not think I need this. With a reverse proxy, the traffic should forwarded through my
  # 80 and 443 ports?
  networking.firewall.allowedTCPPorts = [ port ];
}
