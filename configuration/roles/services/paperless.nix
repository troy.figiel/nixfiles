{ admin, config, lib, machines, terraformOutputs, thisMachine, ... }:

let
  inherit (lib) mkIf mkMerge;
  port = 28981;
  domain = "paperless.local.${terraformOutputs.troyfigiel-domain}";
  secret =
    "${domain}/${config.services.paperless.extraConfig.PAPERLESS_ADMIN_USER}";
in mkMerge [
  (mkIf (thisMachine == machines.nas) {
    # Currently the paperless password can be owned by root, because it will be copied to
    # ${dataDir}/superuser-password by the paperless-copy-password.service, which runs as root.
    lollypops.secrets.files.${secret} = { };

    services.paperless = {
      inherit port;
      enable = true;
      address = "0.0.0.0";
      dataDir = "/storage/services/paperless";
      consumptionDir = "/storage/export/smb/paperless";
      consumptionDirIsPublic = true;
      passwordFile = config.lollypops.secrets.files.${secret}.path;
      extraConfig = {
        PAPERLESS_OCR_LANGUAGE = "deu+eng+nld";
        PAPERLESS_ADMIN_USER = "troyfigiel";
        PAPERLESS_FILENAME_FORMAT = "{created_year}/{correspondent}/{title}";
      };
    };

    # TODO: This is copied from file-share. This needs to be cleaned up, but works for now. What
    # would be a better solution?
    services.samba.shares.paperless = {
      browseable = "yes";
      "read only" = "no";
      "guest ok" = "yes";
      "create mask" = "0644";
      "directory mask" = "0755";
      "force user" = "samba";
      "force group" = "samba";
      path = "/storage/export/smb/paperless";
    };

    services.nginx.virtualHosts.${domain}.locations."/" = {
      proxyPass = "http://${thisMachine.ipAddress}:${toString port}";
      proxyWebsockets = true;
    };
  })

  (mkIf (thisMachine == machines.laptop) {
    fileSystems."/home/${admin.user}/paperless" = {
      device = "//${machines.nas.hostName}/paperless";
      fsType = "cifs";
      options = let
        userConfig = config.users.users.${admin.user};
        inherit (userConfig) uid;
        inherit (config.users.groups.${userConfig.group}) gid;
      in [
        "soft"
        "x-systemd.automount"
        "noauto"
        "uid=${toString uid}"
        "gid=${toString gid}"
      ];
    };
  })
]
