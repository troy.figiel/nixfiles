{ lib, machines, terraformOutputs, thisMachine, ... }:

let
  inherit (lib) mkIf;
  port = 3258;
  domain = "hydra.local.${terraformOutputs.troyfigiel-domain}";
in mkIf (thisMachine == machines.nas) {
  services.hydra = {
    inherit port;
    enable = true;
    hydraURL = "http://${domain}";
    useSubstitutes = true;
    notificationSender = "hydra@localhost";
  };

  services.nginx.virtualHosts.${domain}.locations."/" = {
    proxyPass = "http://${thisMachine.ipAddress}:${toString port}";
    proxyWebsockets = true;
  };
}
