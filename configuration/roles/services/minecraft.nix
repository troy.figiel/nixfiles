{ lib, machines, pkgs, thisMachine, ... }:

let
  inherit (lib) mkIf mkMerge;
  # TODO: This port is currently set in three different places:
  # 1. In my Terraform records (Terraform)
  # 2. In my networking firewall (Nix)
  # 3. In my server properties (file on my NAS)
  # 4. In my Fritz!Box (forwarded port)
  # Can I bring this together somehow? Terranix and declarative server configuration?
  port = 25938;
in mkMerge [
  (mkIf (thisMachine == machines.nas) {
    services.minecraft-server = {
      enable = true;
      eula = true;
      dataDir = "/storage/services/minecraft";
    };

    networking.firewall = {
      allowedTCPPorts = [ port ];
      allowedUDPPorts = [ port ];
    };
  })

  (mkIf (thisMachine == machines.laptop) {
    environment.systemPackages = [ pkgs.prismlauncher ];
  })
]
