{ config, lib, machines, terraformOutputs, thisMachine, ... }:

let
  inherit (lib) mkIf mkMerge;
  port = 5000;
  domain = "harmonia.local.${terraformOutputs.troyfigiel-domain}";
in mkMerge [
  (mkIf (thisMachine == machines.nas) {
    lollypops.secrets.files."${domain}/harmonia.secret" = { };

    services.harmonia = {
      enable = true;
      signKeyPath =
        config.lollypops.secrets.files."${domain}/harmonia.secret".path;
    };

    services.nginx.virtualHosts.${domain}.locations."/" = {
      proxyPass = "http://${thisMachine.ipAddress}:${toString port}";
      proxyWebsockets = true;
    };
  })

  {
    nix.settings = {
      substituters = [ "http://harmonia.local.troyfigiel.com" ];
      trusted-public-keys = [
        "harmonia.local.troyfigiel.com:ribEZXYMAQMEA0JJY+tmC8B6nOthtkBVps0h35jep2k="
      ];
      trusted-users = [ "troy" ];
    };
  }
]
