{ admin, config, lib, machines, thisMachine, ... }:

let inherit (lib) mkIf mkMerge;
in mkMerge [
  (mkIf (thisMachine == machines.nas) {
    services.nfs.server = {
      enable = true;
      exports = ''
        /storage/export/nfs/home ${machines.laptop.ipAddress}(rw,sync,no_subtree_check)
        /storage/export/nfs/home ${admin.vpnIpAddress}(rw,sync,no_subtree_check)
      '';
    };

    # TODO: For some reason on Windows 11, the forced user and group are root. Why is this the
    # case? How do I make this more secure?

    # TODO: In general I need a better security model for SMB. How could I best approach this?
    services.samba = {
      enable = true;
      openFirewall = true;
      extraConfig = ''
        map to guest = bad user
        hosts allow = ${admin.privateCidrBlock}
      '';
      shares = let
        defaultSettings = {
          browseable = "yes";
          "read only" = "no";
          "guest ok" = "yes";
          "create mask" = "0644";
          "directory mask" = "0755";
          "force user" = "samba";
          "force group" = "samba";
        };
      in {
        share = { path = "/storage/export/smb/share"; } // defaultSettings;
        home = { path = "/storage/export/smb/home"; } // defaultSettings;
      };
    };

    users = {
      groups.samba = { };
      users.samba = {
        isNormalUser = true;
        createHome = false;
        group = "samba";
      };
    };

    networking.firewall.allowedTCPPorts = [ 2049 ];
  })

  (mkIf (thisMachine == machines.laptop) {
    fileSystems."/home/${admin.user}/nfs" = {
      device = "${machines.nas.hostName}:/storage/export/nfs/home";
      fsType = "nfs";
      options = [ "soft" "x-systemd.automount" "noauto" ];
    };

    fileSystems."/home/${admin.user}/smb" = {
      device = "//${machines.nas.hostName}/share";
      fsType = "cifs";
      options = let
        userConfig = config.users.users.${admin.user};
        inherit (userConfig) uid;
        inherit (config.users.groups.${userConfig.group}) gid;
      in [
        "soft"
        "x-systemd.automount"
        "noauto"
        "uid=${toString uid}"
        "gid=${toString gid}"
      ];
    };
  })
]
