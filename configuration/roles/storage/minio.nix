{ config, lib, machines, terraformOutputs, thisMachine, ... }:

let
  inherit (lib) mkIf;
  api-port = 9000;
  gui-port = 9001;
  domain = "minio.local.${terraformOutputs.troyfigiel-domain}";
in mkIf (thisMachine == machines.nas) {
  lollypops.secrets.files."minio-credentials" = { };

  services.minio = {
    enable = true;
    browser = true;
    dataDir = [ "/storage/services/minio/data" ];
    configDir = "/storage/services/minio/config";
    rootCredentialsFile =
      config.lollypops.secrets.files."minio-credentials".path;
  };

  services.nginx.virtualHosts.${domain}.locations."/" = {
    proxyPass = "http://${thisMachine.ipAddress}:${toString gui-port}";
    proxyWebsockets = true;
  };

  networking.firewall.allowedTCPPorts = [ api-port gui-port ];
}
