{ admin, config, lib, machines, pkgs, thisMachine, ... }:

let inherit (lib) elem mkIf;
in mkIf (elem thisMachine (with machines; [ laptop nas raspberry-pi ])) {
  # TODO: This is currently being read from the user password in my .local/share. How can I fix
  # this?

  lollypops.secrets.files."root_troyfigiel-gmail-app-password" = {
    cmd = "pass troyfigiel-gmail-app-password";
  };

  programs.msmtp = {
    enable = true;
    accounts.default = {
      auth = "on";
      from = admin.email;
      host = "smtp.gmail.com";
      # TODO: Should I create a separate mail address for my notification sending? This would
      # ensure that even if the account gets compromised, my other email addresses are safe.

      passwordeval = "cat ${
          config.lollypops.secrets.files."root_troyfigiel-gmail-app-password".path
        }";
      port = 465;
      tls = "on";
      # I need to turn off starttls if I use port 465.
      tls_starttls = "off";
      # This is also hardcoded in home-manager. See:
      # https://github.com/nix-community/home-manager/blob/master/modules/accounts/email.nix
      tls_trust_file = "/etc/ssl/certs/ca-certificates.crt";
      user = admin.email;
    };
  };

  systemd.services."failed-service-email@".serviceConfig = {
    ExecStart = let
      systemd-email = pkgs.writeShellApplication {
        name = "systemd-email";
        runtimeInputs = [ pkgs.msmtp ];
        text = ''
          sendmail -t <<ERRMAIL
          To: $1
          From: systemd <root@${thisMachine.hostName}>
          Subject: $2 failed on ${thisMachine.hostName}
          Content-Transfer-Encoding: 8bit
          Content-Type: text/plain; charset=UTF-8

          $(systemctl status --full "$2")
          ERRMAIL
        '';
      };
    in "${systemd-email}/bin/systemd-email ${admin.email} %i";
    Type = "oneshot";
    User = "root";
  };

  systemd.packages = [
    (pkgs.runCommand "onfailure.conf" { } ''
      mkdir -p $out/etc/systemd/system/service.d/
      cat <<EOF > $out/etc/systemd/system/service.d/onfailure.conf
      [Unit]
      OnFailure=failed-service-email@%n.service
      EOF
    '')
  ];
}
