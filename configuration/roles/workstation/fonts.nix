{ admin, lib, machines, pkgs, thisMachine, ... }:

let inherit (lib) mkIf;
in mkIf (thisMachine == machines.laptop) {
  home-manager.users.${admin.user} = {
    home.packages = with pkgs; [
      font-awesome
      iosevka-comfy.comfy-fixed
      # TODO: I added a bunch of these fonts as a hack, because there were quite a few missing.
      # Which fonts do I tend to use?
      (nerdfonts.override { fonts = [ "DroidSansMono" ]; })
      dejavu_fonts
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
    ];
    fonts.fontconfig.enable = true;
  };
}
