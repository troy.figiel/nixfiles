{ admin, lib, machines, pkgs, thisMachine, ... }:

let inherit (lib) mkIf;
in mkIf (thisMachine == machines.laptop) {
  environment.systemPackages = with pkgs; [ gnupg pinentry ];

  home-manager.users.${admin.user} = {
    # TODO: Figure out the details
    # For some reason, these packages are required to make the gnome3 pinentry work.
    home.packages = [ pkgs.pinentry-gnome pkgs.gcr ];

    services.gpg-agent = {
      enable = true;
      enableSshSupport = true;
      enableExtraSocket = true;
      sshKeys = [ "8ABF0116DA24246700017F956358D89FE8B148B8" ];
      pinentryFlavor = "gnome3";
    };

    programs.gpg = {
      enable = true;
      publicKeys = [{
        text = admin.keys.gpg;
        trust = 5;
      }];
    };

    programs.ssh = {
      enable = true;
      forwardAgent = true;
    };
  };
}
