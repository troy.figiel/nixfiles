{ admin, lib, machines, pkgs, thisMachine, ... }:

let inherit (lib) mkIf;
in mkIf (thisMachine == machines.laptop) {
  networking.networkmanager = {
    enable = true;
    plugins = [ pkgs.networkmanager-openvpn ];
  };

  users.users.${admin.user} = { extraGroups = [ "networkmanager" ]; };
}
