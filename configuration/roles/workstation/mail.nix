{ admin, config, lib, machines, pkgs, thisMachine, ... }:

let inherit (lib) elem mkIf mkMerge;
in mkMerge [
  (mkIf (elem thisMachine (with machines; [ laptop nas ])) {
    lollypops.secrets.files = {
      # TODO: We currently need to manually give the lollypops-secrets directory the right
      # permissions. Upon creation it will be owned by root, whereas it should be owned by troy.

      # TODO: Extract .local/share to the appropriate xdg configuration. xdg.shareDir?
      "troyfigiel-gmail-app-password" = {
        owner = admin.user;
        group-name = config.users.users.${admin.user}.group;
      };

      "tfigiel94-gmail-app-password" = {
        owner = admin.user;
        group-name = config.users.users.${admin.user}.group;
      };
    };

    home-manager.users.${admin.user} = {
      programs = {
        msmtp.enable = true;
        notmuch.enable = true;
      };

      accounts.email = let
        gmail-config = {
          gpg = {
            key = "E47C0DCD2768DFA138FCDCD6C67C9181B3893FB0";
            signByDefault = true;
          };
          imap = {
            host = "imap.gmail.com";
            port = 993;
          };
          smtp = {
            host = "smtp.gmail.com";
            port = 465;
          };
          msmtp.enable = true;
          notmuch.enable = true;
          realName = "Troy Figiel";
        };
      in {
        accounts = {
          troyfigiel-gmail = {
            primary = true;
            address = admin.email;
            # TODO: For some reason depending on the path variable for lollypops secrets does not
            # work. This is why I use rec instead.
            passwordCommand = "cat ${
                config.lollypops.secrets.files."troyfigiel-gmail-app-password".path
              }";
            userName = admin.email;
          } // gmail-config;

          tfigiel94-gmail = {
            address = "tfigiel94@gmail.com";
            passwordCommand = "cat ${
                config.lollypops.secrets.files."tfigiel94-gmail-app-password".path
              }";
            userName = "tfigiel94@gmail.com";
          } // gmail-config;
        };
      };
    };
  })

  (mkIf (thisMachine == machines.nas) {
    # TODO: Can I replace this by imapnotify?
    systemd.services."email-sync" = {
      script = ''
        ${pkgs.isync}/bin/mbsync --all
        ${pkgs.notmuch}/bin/notmuch new
      '';
      serviceConfig = {
        Type = "oneshot";
        User = admin.user;
      };
    };

    systemd.timers."email-sync" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnBootSec = "5m";
        OnUnitActiveSec = "5m";
        Unit = "email-sync.service";
      };
    };

    home-manager.users.${admin.user} = {
      programs.mbsync.enable = true;

      accounts.email = {
        maildirBasePath = "/storage/export/nfs/home/applications/mail";

        accounts = {
          troyfigiel-gmail = {
            mbsync = {
              enable = true;
              create = "maildir";
            };
            msmtp.enable = true;
          };

          tfigiel94-gmail = {
            mbsync = {
              enable = true;
              create = "maildir";
            };
          };
        };
      };
    };
  })

  (mkIf (thisMachine == machines.laptop) {
    home-manager.users.${admin.user}.accounts.email.maildirBasePath =
      "/home/${admin.user}/nfs/applications/mail";
  })
]
