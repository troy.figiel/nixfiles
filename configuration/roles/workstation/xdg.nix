{ admin, lib, machines, thisMachine, ... }:

let inherit (lib) mkIf;
in mkIf (thisMachine == machines.laptop) {
  home-manager.users.${admin.user}.xdg = {
    enable = true;
    userDirs = let
      home = "/home/${admin.user}";
      local = "${home}/local";
      nfs = "${home}/nfs";
    in {
      enable = true;
      createDirectories = false;

      # TODO: Clean this up. I can refer to something like config.user.users.troy.home for a
      # better way to refer to config on the same computer.
      desktop = local;
      documents = "${nfs}/documents";
      download = "${local}/downloads";
      music = "${nfs}/audio";
      pictures = "${nfs}/pictures";
      publicShare = "${home}/smb";
      templates = local;
      videos = "${nfs}/videos";
      extraConfig = {
        XDG_PROJECTS_DIR = "${home}/git";
        XDG_MISC_DIR = "${nfs}/misc";
      };
    };
  };
}
