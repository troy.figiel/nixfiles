{ admin, config, inputs, lib, machines, pkgs, thisMachine, ... }:

let
  inherit (lib) mkIf;
  cfg = config.home-manager.users.${admin.user};
in mkIf (thisMachine == machines.laptop) {
  security.pam.services.swaylock = { };

  home-manager = {
    users.${admin.user} = {
      imports = [ inputs.hyprland.homeManagerModules.default ];

      programs.bash.profileExtra = ''
        if [ "$(tty)" = "/dev/tty1" ]; then
           exec Hyprland &> /dev/null
        fi
      '';

      wayland.windowManager.hyprland = {
        enable = true;
        package = inputs.hyprland.packages.${pkgs.system}.default;
        extraConfig = ''
          decoration {
            active_opacity=0.85
            inactive_opacity=0.85
            fullscreen_opacity=1
            blur {
              enabled=true
              size=5
              passes=3
              new_optimizations=true
              ignore_opacity=true
            }
          }

          animations {
            enabled=false
          }

          input {
            kb_layout=de
            kb_options=ctrl:nocaps
            repeat_rate=40
            repeat_delay=300
            touchpad {
              disable_while_typing=false
            }
          }

          misc {
            disable_hyprland_logo=true
            mouse_move_enables_dpms=true
            key_press_enables_dpms=true
          }

          binds {
            workspace_back_and_forth=true
          }

          monitor=,preferred,auto,1
            
          exec-once=${cfg.programs.waybar.package}/bin/waybar
          exec-once=${pkgs.swayidle}/bin/swayidle -w
          exec=${pkgs.swaybg}/bin/swaybg -i ${./nixos.jpg} --mode fill

          bind=SUPER,q,killactive
          bind=SUPERSHIFT,q,exit

          bind=SUPER,l,exec,${cfg.programs.swaylock.package}/bin/swaylock
          bind=SUPER,s,togglefloating

          bind=SUPER,Return,exec,${pkgs.alacritty}/bin/alacritty
          bind=SUPER,space,exec,${cfg.programs.wofi.package}/bin/wofi -S run
          bind=SUPER,e,exec,${cfg.programs.emacs.package}/bin/emacs
          bind=SUPER,b,exec,${cfg.programs.firefox.package}/bin/firefox

          bind=,XF86MonBrightnessUp,exec,${pkgs.brightnessctl}/bin/brightnessctl set 5%+
          bind=,XF86MonBrightnessDown,exec,${pkgs.brightnessctl}/bin/brightnessctl set 5%-
          bind=SHIFT,XF86MonBrightnessUp,exec,${pkgs.brightnessctl}/bin/brightnessctl set 1%+
          bind=SHIFT,XF86MonBrightnessDown,exec,${pkgs.brightnessctl}/bin/brightnessctl set 1%-

          bind=,XF86AudioMute,exec,${pkgs.alsa-utils}/bin/amixer set Master toggle
          bind=,XF86AudioRaiseVolume,exec,${pkgs.alsa-utils}/bin/amixer set Master 5%+
          bind=,XF86AudioLowerVolume,exec,${pkgs.alsa-utils}/bin/amixer set Master 5%-
          bind=SHIFT,XF86AudioRaiseVolume,exec,${pkgs.alsa-utils}/bin/amixer set Master 1%+
          bind=SHIFT,XF86AudioLowerVolume,exec,${pkgs.alsa-utils}/bin/amixer set Master 1%-

          bind=,XF86AudioNext,exec,${pkgs.playerctl}/bin/playerctl next
          bind=,XF86AudioPrev,exec,${pkgs.playerctl}/bin/playerctl previous
          bind=,XF86AudioPlay,exec,${pkgs.playerctl}/bin/playerctl play-pause
          bind=,XF86AudioStop,exec,${pkgs.playerctl}/bin/playerctl stop

          bind=SUPER,left,movefocus,l
          bind=SUPER,down,movefocus,d
          bind=SUPER,up,movefocus,u
          bind=SUPER,right,movefocus,r

          bind=SUPERSHIFT,left,swapwindow,l
          bind=SUPERSHIFT,down,swapwindow,d
          bind=SUPERSHIFT,up,swapwindow,u
          bind=SUPERSHIFT,right,swapwindow,r

          bind=SUPER,f,fullscreen,0
          bind=SUPERSHIFT,f,fullscreen,1

          bind=SUPER,next,workspace,+1
          bind=SUPER,prior,workspace,-1
          bind=SUPERSHIFT,next,movetoworkspace,+1
          bind=SUPERSHIFT,prior,movetoworkspace,-1

          blurls=waybar
        '';
      };

      programs.alacritty = {
        enable = true;
        settings = {
          font = let family = "Iosevka Comfy Fixed";
          in {
            normal = {
              inherit family;
              style = "Regular";
            };

            bold = {
              inherit family;
              style = "Bold";
            };

            italic = {
              inherit family;
              style = "Italic";
            };

            bold_italic = {
              inherit family;
              style = "Bold Italic";
            };

            size = 13;
          };
        };
      };

      programs.waybar = { enable = true; };

      programs.wofi = { enable = true; };

      programs.swaylock = {
        enable = true;
        settings = { image = toString ./nixos.jpg; };
      };

      xdg.configFile."swayidle/config".text = let
        # I need to use daemonize swaylock to ensure I do not spawn multiple locks whenever the
        # timeout hits.
        lockCommand = "${cfg.programs.swaylock.package}/bin/swaylock -f";
        dpmsCommand =
          "${cfg.wayland.windowManager.hyprland.package}/bin/hyprctl dispatch dpms";
      in ''
        timeout 600 '${dpmsCommand} off' resume '${dpmsCommand} on'
        timeout 600 '${lockCommand}'
      '';

      services.dunst = { enable = true; };
    };
  };
}
