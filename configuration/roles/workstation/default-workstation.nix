{ admin, config, inputs, lib, machines, pkgs, thisMachine, ... }:

let inherit (lib) mkIf;
in mkIf (thisMachine == machines.laptop) {
  programs.fuse.userAllowOther = true;

  programs.steam.enable = true;

  environment = {
    systemPackages = with pkgs; [
      brightnessctl
      deadnix
      inxi
      nixfmt
      python3
      statix
      tldr
      udisks2

      # Xdragon is used to create an X-window out of which or into which files can be dragged. For
      # example, xdragon can be used to drag files into a web interface.
      xdragon
    ];
  };

  users.users.${admin.user} = { extraGroups = [ "disk" ]; };
  # TODO: This should be enabled by default in NixOS 23.05.
  services.udisks2.enable = true;

  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;
    extraSpecialArgs = { inherit inputs; };
    users.${admin.user} = {
      home = {
        packages = with pkgs; [
          drawio
          jdupes
          feh
          file
          libreoffice
          lzip
          mpv
          papirus-icon-theme
          signal-desktop
          skypeforlinux
          unar
          unzip
          zip
        ];
      };

      programs.emacs = {
        enable = true;
        package = pkgs.tf-emacs;
      };

      programs.firefox.enable = true;

      programs.password-store = {
        enable = true;
        # TODO: Maybe bind mount password-store to .local/share instead?
        settings.PASSWORD_STORE_DIR = "${
            config.home-manager.users.${admin.user}.xdg.userDirs.extraConfig.XDG_PROJECTS_DIR
          }/gitea.local.troyfigiel.com/troyfigiel/password-store";
      };

      services.mpris-proxy.enable = true;
    };
  };

  services.blueman.enable = true;

  # TODO: This works, but need to check how to configure my printers declaratively in NixOS
  services.printing = {
    enable = true;
    drivers = [ pkgs.epson-escpr2 ];
  };

  # Enable scanning with the scanimage command
  hardware.sane.enable = true;

  # TODO: What does rtkit do?
  security.rtkit.enable = true;
  sound.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  services.avahi = {
    enable = true;
    nssmdns = true;
  };
}
