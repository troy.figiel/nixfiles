{ inputs }:

let
  inherit (inputs.nixpkgs-23_05) lib;
  network = import ./network.nix { inherit lib; };
in (lib.mapAttrs (n: v:
  let inherit (inputs) self;
  in lib.nixosSystem {
    inherit (v) system;
    modules = [
      self.nixosModules

      # TODO: In principle ./hardware/${n}.nix works as well, but statix gives a false positive in
      # this case.
      (./hardware + "/${n}.nix")
    ];

    specialArgs = {
      thisMachine = v;
      inherit inputs self;
      inherit (network) admin machines terraformOutputs;
    };
  })) network.machines
