{ lib }:

rec {
  # We need to use mapAttrs over v.value, because we only need the value of a specific entry in
  # outputs.json. We can safely ignore the `sensitive' and `type' fields.
  terraformOutputs = lib.mapAttrs (_n: v: v.value)
    (builtins.fromJSON (builtins.readFile ../infrastructure/outputs.json));

  admin = {
    user = "troy";
    email = "troy.figiel@gmail.com";
    vpnIpAddress = "192.168.178.201";
    privateCidrBlock = "192.168.178.0/24";
    keys = {
      gpg = builtins.readFile ../keys/troy.pub.asc;
      ssh = builtins.readFile ../keys/troy.pub.ssh;
    };
  };

  machines = {
    laptop = {
      hostName = "laptop";
      ipAddress = "192.168.178.26";
      system = "x86_64-linux";
    };

    raspberry-pi = {
      hostName = "raspberry-pi";
      ipAddress = "192.168.178.30";
      system = "aarch64-linux";
    };

    cloud-server = {
      hostName = "cloud-server";
      ipAddress = terraformOutputs.cloud-server_ip-address;
      system = "x86_64-linux";
    };

    nas = {
      hostName = "nas";
      ipAddress = "192.168.178.37";
      system = "x86_64-linux";
    };
  };
}
