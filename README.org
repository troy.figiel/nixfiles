* Troy's network
** Dependencies between Terraform and Nix
Because I would like my Terraform state file to be backed up, I store it on my NFS storage. This introduces a dependency: First my home network needs to be configured through NixOS, then I can run Terraform and finally I can configure my cloud-server.

Is this a use case for Terranix?
** Disclaimer, be aware!
This is very much a work in progress and (currently) is only intended to be used by me. Although I am cleaning up code along the way and I am aiming to turn the code here into a presentable format, if something works, it is good enough for me at this stage.

Since the lack of Nix examples has been a major stumbling block for me initially, I hope this can pose as an example of what a functioning Nix setup could look like. Some useful references I have been using often (in no particular order):
- https://github.com/hlissner/dotfiles
- https://gitlab.com/NickCao/flakes
- https://github.com/misterio77/nix-config (see also https://github.com/Misterio77/nix-starter-configs)
- https://github.com/Mic92/dotfiles
- https://github.com/mitchellh/nixos-config
** Restic
To mount my Restic repository backed up to Backblaze, run ~AWS_ACCESS_KEY_ID=<secret> AWS_SECRET_ACCESS_KEY=<secret> RESTIC_PASSWORD=<secret> restic mount -r s3:s3.us-west-004.backblazeb2.com/troyfigiel-backup tmp~.
** Roles
What are commonly called modules, I have called roles to clarify the distinction between the modules I define and the modules I import.
** Bootstrapping
What needs to be done, is the following:
- Check whether boot is BIOS or UEFI
- Choose drive to install NixOS on
- Partition drive. 3 partitions for UEFI, 2 for BIOS: nixos, swap and BOOT for UEFI
- Create the file systems and label the partitions. Switch over to btrfs as default instead of ext4?
- Depending on whether the system is completely new (1.) or a reinstall (2.)
  1. Run nixos-generate config, replace the generated configuration by one that has openssh enabled and run nixos-install --no-root-passwd.
  2. Install the system from a flake directly.

I could turn this into an interactive installer, asking these questions.

Two scripts:
- format (disks) with options type ("legacy" and "uefi") and disk (e.g. /dev/sda)
- install with parameter either one of my nixos configurations (cloud-server, etc.) or nothing. With nothing, it will install a "default" set up I can ssh into as root.
** Installing NixOS on my Raspberry
Following steps:
- Download an SD ISO for NixOS
- Add my ssh public key to /root/.ssh/authorized_keys on the ISO
- Update firmware as per the guide
- Download a simple configuration
- Run nixos-install --root /
- Finally, reboot!

References:
- https://nixos.org/guides/installing-nixos-on-a-raspberry-pi.html
- https://github.com/NixOS/nixpkgs/pull/96991
