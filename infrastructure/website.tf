resource "cloudflare_record" "troyfigiel" {
  zone_id = cloudflare_zone.troyfigiel.id
  name    = cloudflare_zone.troyfigiel.zone
  value   = vultr_instance.cloud-server.main_ip
  type    = "A"
  ttl     = 3600
}

resource "cloudflare_record" "www" {
  zone_id = cloudflare_zone.troyfigiel.id
  name    = "www.${cloudflare_zone.troyfigiel.zone}"
  value   = cloudflare_zone.troyfigiel.zone
  type    = "CNAME"
  ttl     = 3600
}

resource "cloudflare_record" "local" {
  zone_id = cloudflare_zone.troyfigiel.id
  name    = "*.local.${cloudflare_zone.troyfigiel.zone}"
  value   = "192.168.178.37"
  type    = "A"
  ttl     = 3600
}

# This record is dynamically updated by cloudflare-dyndns.service.
data "cloudflare_record" "home" {
  zone_id  = cloudflare_zone.troyfigiel.id
  hostname = "home.${cloudflare_zone.troyfigiel.zone}"
  type     = "A"
}

resource "cloudflare_record" "_minecraft_tcp" {
  zone_id = cloudflare_zone.troyfigiel.id
  name    = "_minecraft_tcp"
  type    = "SRV"
  ttl     = 3600

  data {
    service  = "_minecraft"
    proto    = "_tcp"
    name     = "minecraft.troyfigiel.com"
    priority = 0
    weight   = 0
    port     = 25938
    target   = "home.${cloudflare_zone.troyfigiel.zone}"
  }
}
