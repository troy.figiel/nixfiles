# TODO: This contains the location of my NFS share hardcoded. We need to fix this somehow.
terraform {
  backend "local" { path = "/home/troy/nfs/applications/terraform/terraform.tfstate" }

  required_providers {
    b2         = { source = "Backblaze/b2" }
    cloudflare = { source = "cloudflare/cloudflare" }
    vultr      = { source = "vultr/vultr" }
  }
}

provider "b2" {}
provider "cloudflare" {}
provider "vultr" {}
