output "cloud-server_ip-address" {
  description = "Public IP address of my Vultr server."
  value       = vultr_instance.cloud-server.main_ip
}

output "restic-bucket" {
  description = "Name of the bucket storing all my Restic backups."
  value       = b2_bucket.restic.bucket_name
}

output "troyfigiel-domain" {
  description = "Domain of my Cloudflare zone."
  value       = cloudflare_zone.troyfigiel.zone
}
