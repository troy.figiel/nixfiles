resource "vultr_iso_private" "nixos-23_05" {
  url = "https://channels.nixos.org/nixos-23.05/latest-nixos-minimal-x86_64-linux.iso"
}

resource "vultr_instance" "cloud-server" {
  plan             = "vc2-1c-1gb"
  region           = "fra"
  activation_email = false
  ddos_protection  = false
  label            = "cloud-server"
  iso_id           = vultr_iso_private.nixos-23_05.id
}

# resource "vultr_reverse_ipv4" "cloud-server_reverse_ipv4" {
#   instance_id = vultr_instance.cloud-server.id
#   ip          = vultr_instance.cloud-server.main_ip
#   reverse     = "mail.${cloudflare_zone.troyfigiel.zone}"
# }
