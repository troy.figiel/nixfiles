data "b2_account_info" "account" {}

resource "b2_bucket" "restic" {
  bucket_name = "troyfigiel-restic"
  bucket_type = "allPrivate"
  lifecycle_rules {
    # An empty string means this policy should apply to all files.
    # See: https://github.com/Backblaze/terraform-provider-b2/issues/3
    file_name_prefix              = ""
    days_from_hiding_to_deleting  = 30
    days_from_uploading_to_hiding = 0
  }
}
